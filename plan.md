## base type
- Bool
- String
## type
### define Integer type like
```
	foo is Integer
		foo's minValue is 1
		foo's maxValue is 100
```
### define Array type like
```
	foo is Array
		foo's minLength is 1
		foo's maxLength is 100
```
### define Tuple type like
```
	foo is Tuple
		foo addType Bool
		foo addType String
```
### define Struct type like
```
	foo is Struct
		foo addfield firstField
		foo's firstField's type is Bool
		foo addfield secondField
		foo's secondField's type is String
```
### define Enum type like 
```
	foo is Enum
		foo addfield firstField
		foo's firstField's type is Bool
		foo addfield secondField
		foo's secondField's type is String
```
## function
### define Function
```
	plus is Function
		plus's left's type is Integer
		plus's right's type is Integer
		plus's result's type is Integer
		plus's left add plus's right setTo plus's result
	
	ifTrue is Function
		ifTrue's left's type is Bool
		ifTrue's right's type is Integer
		ifTrue's result's type is Integer
		ifTrue's left then ifTrue's right setTo ifTrue's result
		false equal ifTrue's left then 0 setTo ifTrue's result

```

